using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;
using Unity.Cinemachine;

namespace TamarilloTools
{
    /// <summary>
    /// THis class will handle the controls for the third person character.
    /// This means there is going to be a requirement for an Input
    /// </summary>
    public class OP_ThirdPersonPlayerController : MonoBehaviour
    {

        [BoxGroup("Input Actions")] public InputActionReference moveAction;
        [BoxGroup("Input Actions")] public InputActionReference lookAction;
        [BoxGroup("Input Actions")] public InputActionReference runAction;
        [BoxGroup("Input Actions")] public InputActionReference sprintAction;
        /// <summary>
        /// Movement for the player. Can be overriden using the property MovementSpeed.
        /// </summary>
        [BoxGroup("Player Stats")][SerializeField] private float movementSpeed = 10f;
        [BoxGroup("Camera")] public CinemachineFreeLook freelookCamera;
        [BoxGroup("Character")] public GameObject characterGameobject;

        protected Camera mainCameraReference;
        protected OP_Character characterReference;
        public float MovementSpeed { get => movementSpeed; set => movementSpeed = value; }

        // Start is called before the first frame update
        protected virtual void Start()
        {
            // This should not be used as the real camera as it lacks functionality which can only be really tweaked in editor
            // Might be worth exploring doing a camera system so that it can handle more settings
            SetupCamera();
            if (characterGameobject == null)
            {
                characterGameobject = gameObject;
                characterReference = characterGameobject.AddComponent<OP_Character>();
            }
            else
            {
                characterReference = characterGameobject.GetComponentInChildren<OP_Character>();
            }
            characterReference.SetCameraTransform(mainCameraReference.transform);
            SetupActionBindings();

        }

        private void SetupCamera()
        {
            mainCameraReference = Camera.main;
            mainCameraReference.gameObject.AddComponent<CinemachineBrain>();
            if (freelookCamera == null)
            {
                GameObject cameraObject = new GameObject("Freelook Camera");
                freelookCamera = cameraObject.AddComponent<CinemachineFreeLook>();
                CinemachineInputProvider provider = cameraObject.AddComponent<CinemachineInputProvider>();
                provider.XYAxis = lookAction;
                freelookCamera.m_LookAt = characterGameobject.transform;
                freelookCamera.m_Follow = characterGameobject.transform;
                freelookCamera.m_BindingMode = Unity.Cinemachine.TargetTracking.BindingMode.LockToTargetOnAssign;
            }
        }

        private void SetupActionBindings()
        {
            if (moveAction == null) Debug.LogError("The Move Action was not assigned in the OP_ThirdPersonPlayerController");
            OP_InputManager.GetInputInstance().BindInputActionPerformed(moveAction,
                (ctx) =>
                {
                    if (characterReference.DoesCharacterAccelerate)
                    {
                        characterReference.ToggleIsMoving(true);
                    }
                });
            OP_InputManager.GetInputInstance().BindInputActionCanceled(moveAction,
                (ctx) =>
                {
                    if (characterReference.DoesCharacterAccelerate)
                    {
                        characterReference.ToggleIsMoving(false);
                    }
                    else
                    {
                        characterReference.SetMoveVector(Vector2.zero);
                    }
                });
            if(runAction != null)
            {
                OP_InputManager.GetInputInstance().BindInputActionPerformed(runAction, ctx => { characterReference.IsRunning = true; });
                OP_InputManager.GetInputInstance().BindInputActionCanceled(runAction, ctx => { characterReference.IsRunning = false; });
            }

            if (sprintAction != null)
            {
                OP_InputManager.GetInputInstance().BindInputActionPerformed(sprintAction, ctx => { characterReference.IsSprinting = true; });
                OP_InputManager.GetInputInstance().BindInputActionCanceled(sprintAction, ctx => { characterReference.IsSprinting = false; });
            }
            
        }

        protected virtual void OnEnable()
        {
            EnablePlayerControllerInput();
        }

        protected virtual void OnDisable()
        {
            DisablePlayerControllerInput();
        }

        protected virtual void Update()
        {
            // We want to drive the acceleration of the character by using the last vector value input
            if (moveAction.ToInputAction().ReadValue<Vector2>() != Vector2.zero)
            {
                characterReference.SetMoveVector(moveAction.ToInputAction().ReadValue<Vector2>());
            }
        }

        protected virtual void EnablePlayerControllerInput()
        {
            moveAction?.ToInputAction().Enable();
            lookAction?.ToInputAction().Enable();
            runAction?.ToInputAction().Enable();
            sprintAction?.ToInputAction().Enable();
        }
        protected virtual void DisablePlayerControllerInput()
        {
            moveAction?.ToInputAction().Disable();
            lookAction?.ToInputAction().Disable();
            runAction?.ToInputAction().Disable();
            sprintAction?.ToInputAction().Disable();
        }
    }
}
