using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TamarilloTools
{
    /// <summary>
    /// This class is gonig to be in charge of handling adding and removing input bindings. 
    /// But it is responsibility of each project to handle activation and deactivation of the input given situations
    /// </summary>
    public class OP_InputManager : MonoBehaviour
    {
        public static OP_InputManager Instance { get; private set; }

        public PlayerInput PlayerInput { get; private set; }
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }
            Instance = this;

            PlayerInput = GetComponent<PlayerInput>();
            if (PlayerInput == null)
            {
                PlayerInput = gameObject.AddComponent<PlayerInput>();
            }
        }

        public static OP_InputManager GetInputInstance()
        {
            if (Instance == null)
            {
                GameObject inputManagerGameobject = new GameObject("Input Manager");
                inputManagerGameobject.AddComponent<OP_InputManager>();
            }
            return Instance;
        }

        public void BindInputActionStarted(InputActionReference input, Action<InputAction.CallbackContext> callback) => input.ToInputAction().started += callback;
        public void UnbindInputActionStarted(InputActionReference input, Action<InputAction.CallbackContext> callback) => input.ToInputAction().started -= callback;
        public void BindInputActionPerformed(InputActionReference input, Action<InputAction.CallbackContext> callback) => input.ToInputAction().performed += callback;
        public void UnbindInputActionPerformed(InputActionReference input, Action<InputAction.CallbackContext> callback) => input.ToInputAction().performed -= callback;
        public void BindInputActionCanceled(InputActionReference input, Action<InputAction.CallbackContext> callback) => input.ToInputAction().canceled += callback;
        public void UnbindInputActionCanceled(InputActionReference input, Action<InputAction.CallbackContext> callback) => input.ToInputAction().canceled -= callback;
    }
}
