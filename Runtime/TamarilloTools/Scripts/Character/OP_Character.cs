using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TamarilloTools;
using UnityEngine;

public class OP_Character : MonoBehaviour
{
    public static float SPEED_DELTA_TOLERANCE = 0.1f;

    [BoxGroup("Movement Settings")][SerializeField] protected bool movementFollowsCameraAlways = true;
    [BoxGroup("Movement Settings")][SerializeField] protected bool doesCharacterAccelerate = true;
    [BoxGroup("Movement Settings")][SerializeField] protected bool waitForTurnToStartMovement = true;
    [InfoBox("The character stats are meant for testing. You should populate these with data from other sources")]
    [BoxGroup("Character Stats")][SerializeField] protected float movementSpeed = 10f;
    [BoxGroup("Character Stats")][SerializeField] protected float runMovementSpeed = 15f;
    [BoxGroup("Character Stats")][SerializeField] protected float sprintMovementSpeed = 20f;
    [ShowIf("doesCharacterAccelerate")][BoxGroup("Character Stats")][SerializeField] protected float acceleration = 0.1f;
    [BoxGroup("Character Stats")][SerializeField] protected float rotationSpeed = 300f;
    /// <summary>
    /// The movement vector used for movign the character
    /// </summary>
    protected Vector3 moveVector;
    protected bool isMoving;
    protected bool isRunning;
    protected bool isSprinting;
    protected float currentMovementSpeed;
    protected float currentMaxMovementSpeed;
    protected Transform cameraTransformReference;

    public bool DoesCharacterAccelerate { get => doesCharacterAccelerate; }
    public bool IsMoving { get => isMoving; }
    public bool IsRunning { get => isRunning; set => isRunning = value; }
    public bool IsSprinting { get => isSprinting; set => isSprinting = value; }

    protected virtual void LateUpdate()
    {
        MoveCharacter();
        RotateCharacter();
    }
    public virtual void MoveCharacter()
    {
        HandleMovementSpeed();
        float factor = Mathf.Max(0, Vector3.Dot(moveVector, transform.forward));
        if (waitForTurnToStartMovement && currentMovementSpeed < currentMaxMovementSpeed - SPEED_DELTA_TOLERANCE)
        {
            factor = factor >= 0.9f ? 1 : 0;
        }

        transform.position += moveVector * factor * Time.deltaTime * currentMovementSpeed;
    }

    private void HandleMovementSpeed()
    {
        currentMaxMovementSpeed = movementSpeed;
        if (isSprinting)
        {
            currentMaxMovementSpeed = sprintMovementSpeed;
        }
        else if (isRunning)
        {
            currentMaxMovementSpeed = runMovementSpeed;
        }

        if (doesCharacterAccelerate) 
        {
            float x = currentMovementSpeed / currentMaxMovementSpeed;

            float t = MathUtils.InverseEaseInQuart(x);

            if(isMoving)
            {
                t += Time.deltaTime * acceleration;
            }
            else
            {
                t -= Time.deltaTime * acceleration;
            }
            

            x = MathUtils.EaseInQuart(t);

            currentMovementSpeed = x * currentMaxMovementSpeed;
        }
        else
        {
            currentMovementSpeed = movementSpeed;
            if(isSprinting)
            {
                currentMovementSpeed = sprintMovementSpeed;
            }
            else if (isRunning)
            {
                currentMovementSpeed = runMovementSpeed;
            }
        }

        float angle = Vector3.Angle(transform.forward, moveVector);

        if (angle > 179 || angle < -179) currentMovementSpeed = 0;
    }

    public virtual void RotateCharacter()
    {
        if (moveVector != Vector3.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(moveVector, Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
        }
    }


    public void SetMoveVector(Vector3 newMove)
    {
        moveVector = newMove;


        if (movementFollowsCameraAlways)
        {
            moveVector = moveVector.x * cameraTransformReference.right + moveVector.y * cameraTransformReference.forward;
        }

        moveVector = Vector3.ProjectOnPlane(moveVector, Vector3.up);
        moveVector.Normalize();
    }
    public void SetCameraTransform(Transform newCameraTransform) => cameraTransformReference = newCameraTransform;
    public void ToggleIsMoving(bool newValue) => isMoving = newValue;
}
