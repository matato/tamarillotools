﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace TamarilloTools
{
    public class CoroutineHandler : MonoBehaviour
    {
        /// <summary>
        /// IEnumerator for ExecuteActionAfter
        /// </summary>
        /// <param name="action"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        private static IEnumerator ExecuteActionAfterEnumerator(Action action, float duration)
        {
            yield return new WaitForSeconds(duration);
            action?.Invoke();
        }

        /// <summary>
        /// This method will execute a coroutine passed by parameter after the duration passed by parameter
        /// </summary>
        /// <param name="action"> Method to execute </param>
        /// <param name="duration"> Time until the action is executed </param>
        /// <param name="objectOwner"> Which monobehaviour is instantiating the coroutine </param>
        public static void ExecuteActionAfter(MonoBehaviour objectOwner, Action action, float duration)
        {
            objectOwner.StartCoroutine(ExecuteActionAfterEnumerator(action, duration));
        }

        /// <summary>
        /// A "virtual" update where an action is repeated until the condition passed by paramter is met
        /// </summary>
        /// <param name="actionToRepeat"></param>
        /// <param name="exitCondition"></param>
        /// <returns></returns>
        private static IEnumerator UpdateUntilEnumerator(Action actionToRepeat, bool exitCondition)
        {
            while (!exitCondition)
            {
                actionToRepeat?.Invoke();
                yield return null;
            }
        }

        /// <summary>
        /// IEnumerator for UpdateUntil
        /// </summary>
        /// <param name="actionToRepeat"></param>
        /// <param name="exitCondition"></param>
        /// <param name="objectOwner"></param>
        public static void UpdateUntil(MonoBehaviour objectOwner, Action actionToRepeat, bool exitCondition)
        {
            objectOwner.StartCoroutine(UpdateUntilEnumerator(actionToRepeat, exitCondition));
        }


        /// <summary>
        /// IEnumerator for ExecuteActionAfterUpdateUntil
        /// </summary>
        /// <param name="actionToRepeat"></param>
        /// <param name="exitCondition"></param>
        /// <param name="actionAfter"></param>
        /// <returns></returns>
        private static IEnumerator ExecuteActionAfterUpdateUntilEnumerator(Func<bool> actionToRepeat, Action actionAfter = null)
        {
            bool exitCondition = false;
            Assert.IsNotNull(actionToRepeat);
            while (!exitCondition)
            {
                exitCondition = (bool)(actionToRepeat?.Invoke());
                yield return null;
            }
            actionAfter?.Invoke();
        }

        /// <summary>
        /// A "virtual" update where an action is repeated until the condition passed by paramter is met
        /// </summary>
        /// <param name="actionToRepeat"></param>
        /// <param name="objectOwner"></param>
        /// <param name="actionAfter"></param>
        public static void ExecuteActionAfterUpdateUntil(MonoBehaviour objectOwner, Func<bool> actionToRepeat, Action actionAfter = null)
        {
            objectOwner.StartCoroutine(ExecuteActionAfterUpdateUntilEnumerator(actionToRepeat, actionAfter));
        }

        /// <summary>
        /// Enumerator for RepeatAction
        /// </summary>
        /// <param name="repeatableAction"></param>
        /// <param name="exitCondition"></param>
        /// <param name="maxTimeBetweenRepeats"></param>
        /// <returns></returns>
        private static IEnumerator RepeatActionEnumerator(Action repeatableAction, int timesToExecute, float maxTimeBetweenRepeats, bool randomTime)
        {
            int countTimes = 0;
            float realTimeBetweenRepeats = maxTimeBetweenRepeats;

            //If it is set to be a randomTime then change the time 
            realTimeBetweenRepeats = randomTime ? UnityEngine.Random.Range(0.1f, maxTimeBetweenRepeats) : realTimeBetweenRepeats;
            while (countTimes < timesToExecute)
            {
                yield return new WaitForSeconds(realTimeBetweenRepeats);
                repeatableAction?.Invoke();
                //Change the time if random
                realTimeBetweenRepeats = randomTime ? UnityEngine.Random.Range(0.1f, maxTimeBetweenRepeats) : realTimeBetweenRepeats;
                countTimes++;
                yield return null;
            }
        }

        /// <summary>
        /// This method will execute an action a number of times with a set interval of time in between them until the number of executions is met.
        /// If randomTime is true, the interval will be a random number between 0.1f and the maxTimeBetweenRepeats
        /// </summary>
        /// <param name="objectOwner"></param>
        /// <param name="repeatableAction"></param>
        /// <param name="timesToExecute"></param>
        /// <param name="maxTimeBetweenRepeats"></param>
        /// <param name="randomTime"></param>
        public static void RepeatAction(MonoBehaviour objectOwner, Action repeatableAction, int timesToExecute, float maxTimeBetweenRepeats, bool randomTime = false)
        {
            objectOwner.StartCoroutine(RepeatActionEnumerator(repeatableAction, timesToExecute, maxTimeBetweenRepeats, randomTime));
        }

        /// <summary>
        /// Enumerator for TimedUpdate
        /// </summary>
        /// <param name="repeatableAction"></param>
        /// <param name="exitCondition"></param>
        /// <param name="maxTimeBetweenRepeats"></param>
        /// <returns></returns>
        public static IEnumerator TimedUpdateEnumerator(Action repeatableAction, float timesInSeconds)
        {
            float lastUpdate = Time.time;

            while (timesInSeconds > Time.time - lastUpdate)
            {
                repeatableAction?.Invoke();
                yield return null;
            }
        }
        /// <summary>
        ///  This method will execute an action for a set period of time. 
        ///  Think of it like an Update that stops running after a certain amount of seconds have passed
        /// </summary>
        /// <param name="objectOwner"></param>
        /// <param name="repeatableAction"></param>
        /// <param name="timesInSeconds"></param>
        public static void TimedUpdate(MonoBehaviour objectOwner, Action repeatableAction, float timesInSeconds)
        {
            objectOwner.StartCoroutine(TimedUpdateEnumerator(repeatableAction, timesInSeconds));
        }
    }
}
