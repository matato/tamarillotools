﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using TMPro;

namespace TamarilloTools
{
    public class LoadingScreenManager : MonoBehaviour
    {
        [Header("Loading Visuals")]
        public Image loadingIcon;
        public Button loadingDoneButtonn;
        public TextMeshProUGUI loadingText;
        public Image progressBar;

        [Header("Timing Settings")]
        public float waitOnLoadEnd = 0.25f;

        [Header("Loading Settings")]
        public LoadSceneMode loadSceneMode = LoadSceneMode.Additive;
        public ThreadPriority loadThreadPriority;
        public bool shouldShowVisuals = false;


        private AsyncOperation operation;
        private Scene currentScene;
        protected FadeUI fadeHandler;

        public static int sceneToLoad = 1;
        // IMPORTANT! This is the build index of your loading scene. You need to change this to match your actual scene index
        public static int loadingSceneIndex = 0;

        public static string SceneNameFromIndex(int BuildIndex)
        {
            string path = SceneUtility.GetScenePathByBuildIndex(BuildIndex);
            int slash = path.LastIndexOf('/');
            string name = path.Substring(slash + 1);
            int dot = name.LastIndexOf('.');
            return name.Substring(0, dot);
        }

        public static int SceneIndexFromName(string sceneName)
        {
            for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
            {
                string testedScreen = SceneNameFromIndex(i).Trim().ToLower();
                if (testedScreen.Equals(sceneName.Trim().ToLower())) return i;
            }
            return -1;
        }
        public static void LoadSceneByIndex(int levelNum)
        {
            sceneToLoad = levelNum;
            SceneManager.LoadSceneAsync(loadingSceneIndex);
        }

        public static void LoadSceneByName(string sceneName)
        {
            Application.backgroundLoadingPriority = UnityEngine.ThreadPriority.High;
            sceneToLoad = SceneIndexFromName(sceneName);
            SceneManager.LoadSceneAsync(loadingSceneIndex);
        }

        protected virtual void Start()
        {
            Time.timeScale = 1;
            if (sceneToLoad < 0)
                return;

            fadeHandler = gameObject.AddComponent<FadeUI>(); // Making sure it's on so that we can crossfade Alpha
            currentScene = SceneManager.GetActiveScene();
            StartCoroutine(LoadAsync(sceneToLoad));
        }


        protected virtual IEnumerator LoadAsync(int levelNum)
        {
            if (shouldShowVisuals)
                ShowLoadingVisuals();

            yield return new WaitForSeconds(FadeUI.GetFadeDuration());

            StartOperation(levelNum);

            float lastProgress = 0f;

            // operation does not auto-activate scene, so it's stuck at 0.9
            while (DoneLoading() == false)
            {
                yield return null;

                if (Mathf.Approximately(operation.progress, lastProgress) == false)
                {
                    //progressBar.fillAmount = operation.progress;
                    lastProgress = operation.progress;
                }
            }

            if (shouldShowVisuals)
                ShowCompletionVisuals();

            yield return new WaitForSeconds(waitOnLoadEnd);

            yield return new WaitForSeconds(FadeUI.GetFadeDuration());

            FinishLoading();

        }

        protected virtual void StartOperation(int levelNum)
        {
            Application.backgroundLoadingPriority = loadThreadPriority;
            operation = SceneManager.LoadSceneAsync(levelNum, loadSceneMode);


            if (loadSceneMode == LoadSceneMode.Single)
                operation.allowSceneActivation = false;
        }

        protected virtual bool DoneLoading()
        {
            //return operation.isDone;
            return (loadSceneMode == LoadSceneMode.Additive && operation.isDone) || (loadSceneMode == LoadSceneMode.Single && operation.progress >= 0.9f);
        }

        protected virtual void ShowLoadingVisuals()
        {
            loadingIcon.gameObject.SetActive(true);
            loadingDoneButtonn.gameObject.SetActive(false);

            progressBar.fillAmount = 0f;
        }

        protected virtual void ShowCompletionVisuals()
        {
            StopAllCoroutines();
            loadingIcon.gameObject.SetActive(false);
            loadingDoneButtonn.gameObject.SetActive(true);
        }

        protected virtual void FinishLoading()
        {
            if (loadSceneMode == LoadSceneMode.Additive)
                SceneManager.UnloadSceneAsync(currentScene.name);
            else
                operation.allowSceneActivation = true;
        }
    }
}
