//Copyright � 2020 Tamarillo Games, Omar Pino

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System;

namespace TamarilloTools
{   // TODO Find a way to reuse already created objects to avoid reinitialization of gameobjects which could be expensive
    // Not done in first iteration to avoid delays in features
    /// <summary>
    /// The UIStackSystem is to be used to pull and push UI elements to an existing canvas in a way where 
    /// every element in the UI is know to the system and can be added and removed sothe Canvas on the scene 
    /// doesn't have items inside that might bloat the UI and intervene with each other in undesired ways.
    /// 
    /// Unfortunately, given how the UNITYUI system works we can't initiate the system once a scene has been 
    /// loaded. Which would make it easier for a manager to push and pull from the Stack. Instead, this will 
    /// a singletonm while independent managers will handle interactions  and hold references to prefabs, 
    /// scriptableObjects, etc...
    /// </summary>
    public sealed class UIStackSystem
    {
        /// <summary>
        /// Creating a singleton pattern using a simpel thread safety code. 
        /// As explained in this code https://csharpindepth.com/Articles/Singleton
        /// </summary>
        private static UIStackSystem instance = null;
        private static readonly object padlock = new object();

        public static UIStackSystem Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new UIStackSystem();
                    }
                    return instance;
                }
            }
        }

        public enum UILoadMode
        {
            ContentScreen,  // The last object in the stack will be disabled, it will add and activate the new one
            Overlay         // The object will appear on top of the UI without deactivating anything in the stack
        }

        private static Stack<GameObject> userInterfaceStack;
        private static List<GameObject> overlayList;

        UIStackSystem()
        {
            userInterfaceStack = new Stack<GameObject>();
            overlayList = new List<GameObject>();
        }

        /// <summary>
        /// Helper class to open the different screens
        /// </summary>
        /// <param name="owner">The canvas where the object will be placed</param>
        /// <param name="uiToAdd">The prefab of the screen to add</param>
        /// <param name="loadMode">Decides if the screen is loaded into the stack or as an overlay</param>
        /// <returns> Return the instance created in the stack</returns>
        /// <exception cref="Exception"></exception>
        private GameObject OpenUI(GameObject owner, GameObject uiToAdd, UILoadMode loadMode = UILoadMode.ContentScreen)
        {
            if (owner == null) throw new Exception("There is no object in to which place the new UI screen.");
            if (uiToAdd == null) throw new Exception("No UI screen was passed to instantiate.");

            GameObject stackableUI = UnityEngine.Object.Instantiate(uiToAdd, owner.transform);

            switch (loadMode)
            {
                case UILoadMode.ContentScreen:
                    {
                        // Disables the previous screen loaded in the stack
                        if (userInterfaceStack.Count > 0)
                        {
                            userInterfaceStack.Peek().gameObject.SetActive(false);
                        }

                        userInterfaceStack.Push(stackableUI);
                    }
                    break;
                case UILoadMode.Overlay:
                    {
                        overlayList.Add(stackableUI);
                    }
                    break;
                default:
                    throw new Exception("SHOULD NEVER TRIGGER THIS");
            }

            return stackableUI;

        }
        /// <summary>
        /// Opens the screen as a content screen meaning it will close the previous screen in the UI
        /// </summary>
        /// <param name="owner">The canvas where the object will be placed</param>
        /// <param name="uiToAdd">The prefab of the screen to add</param>
        /// <returns> Return the instance created in the stack</returns>
        public GameObject OpenContentScreen(GameObject owner, GameObject uiToAdd)
        {
            return OpenUI(owner, uiToAdd, UILoadMode.ContentScreen);
        }
        /// <summary>
        /// Opens the screen as Overlay. The screen will appear on top of existing UI
        /// </summary>
        /// <param name="owner">The canvas where the object will be placed</param>
        /// <param name="uiToAdd">The prefab of the screen to add</param>
        /// <returns> Return the instance created in the stack</returns>
        public GameObject OpenOverlay(GameObject owner, GameObject uiToAdd)
        {
            return OpenUI(owner, uiToAdd, UILoadMode.Overlay);
        }

        /// <summary>
        /// Closes the last screen opened from the stack, removes it from the stack and activates the previous screen if there is one
        /// If there are overelay screens open. It will close all of them before going back.
        /// TODO Have a way to take into account posible animations between screen transitions before destroying a screen.
        /// </summary>
        public void GoBackToPreviousScreen(bool closeLastScreen = true)
        {
            if (userInterfaceStack.Count == 0) throw new Exception("The UIStack is empty");
            if (userInterfaceStack.Count == 1 && closeLastScreen) Debug.LogWarning("No screen to go back to. Current screen will be closed but nothing will be activated");
            else if (userInterfaceStack.Count == 1 && closeLastScreen == false) return;

            GameObject screenToClose = userInterfaceStack.Pop();
            if (screenToClose != null)
            {
                UnityEngine.Object.Destroy(screenToClose);
            }

            if (userInterfaceStack.Count == 0) return;
            GameObject screenToReopen = userInterfaceStack.Peek();
            if (screenToReopen != null)
            {
                screenToReopen.gameObject.SetActive(true);
            }
            CloseAllOverlayScreens();
        }
        /// <summary>
        /// Closes all the screens in the stack and empties the stack.
        /// Mainly used for unloading the stack when exiting a scene
        /// </summary>
        public void CloseAllContentScreens()
        {
            while (userInterfaceStack.Count > 0)
            {
                GameObject screen = userInterfaceStack.Pop();
                if (screen != null)
                {
                    UnityEngine.Object.Destroy(screen);
                }
            }
            GC.Collect();
        }
        /// <summary>
        /// Closes the last overlay that was opened on the current screen
        /// </summary>
        /// <exception cref="Exception">Throws exception if the list is empty</exception>
        public void CloseLastOverlayScreen()
        {
            if (overlayList.Count == 0) throw new Exception("CloseLastOverlayScreen::There are no overlay screens to close");

            UnityEngine.Object.Destroy(overlayList[overlayList.Count - 1]);
            overlayList.RemoveAt(overlayList.Count - 1);
        }
        /// <summary>
        /// Closes all the screens taht were added as overlays
        /// </summary>
        public void CloseAllOverlayScreens()
        {
            for (int i = 0; i < overlayList.Count; i++)
            {
                UnityEngine.Object.Destroy(overlayList[i]);
            }
            GC.Collect();
        }

        /// <summary>
        /// Removes a screen from the Overlay list without closing anything else. 
        /// </summary>
        /// <param name="overlayToRemove"> The overlay screen to remove </param>
        /// <exception cref="Exception"> If the overlay doesn't exit it will fail.</exception>
        public void CloseOverlayScreen(GameObject overlayToRemove)
        {
            if (overlayList.Count == 0 || overlayToRemove == null) throw new Exception("CloseOverlayScreen::There are no overlay screens to close");

            UnityEngine.Object.Destroy(overlayList.Find((g) => { return g == overlayToRemove; }));
            overlayList.Remove(overlayToRemove);
        }
    }
}
