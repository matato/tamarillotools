using NaughtyAttributes;
using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace TamarilloTools
{
    /// <summary>
    /// This class creates a Fade Iamge on top the canvas to do black to transparent and viceversa.
    /// This is to allow things to load and not show immediatlyon screen.
    /// 
    /// Alternatively, this could be changed so the image is created by an extension of the Canvas class.
    /// This is to ensure we don't need other dependencies and static classes all over the place. But this needs to be tested first.
    /// </summary>
    public class FadeUI : MonoBehaviour
    {
        [SerializeField] private float fadeDuration = 1f;
        [SerializeField] private Color fadeImageColor = Color.black;
        private float waitTimeToInitFade = 1f;
        public static float StaticFadeDuration = 1f;
        public static Color StaticFadeImageColor = Color.black;

        [Foldout("Fade In Events")]
        public UnityEvent OnFadeInStarted;
        [Foldout("Fade In Events")]
        public UnityEvent OnFadeInFinished;
        [Foldout("Fade Out Events")]
        public UnityEvent OnFadeOutStarted;
        [Foldout("Fade Out Events")]
        public UnityEvent OnFadeOutFinished;

        private static Canvas fadeCanvas;
        private static Image fadeTween;
        /// <summary>
        /// Creates a Fade Image 
        /// </summary>
        private static void CreateFadeImage()
        {
            GetCanvas();

            if (fadeCanvas == null) throw new Exception("No Canvas found. No Fade UI will be created.");
            fadeCanvas.sortingOrder = 10; // ensuring its on top of every other canvas. TODO Change to use a constant for sorting order max

            GameObject fadeImageGo = new GameObject("FadeImage", typeof(Image));
            fadeTween = fadeImageGo.GetComponent<Image>();
            fadeTween.color = StaticFadeImageColor;
            fadeTween.raycastTarget = false;  // Ensure the fade panel won't block input
            fadeTween.maskable = false;
            fadeTween.transform.SetAsLastSibling();

            fadeImageGo.layer = LayerMask.NameToLayer("UI");
            fadeImageGo.transform.SetParent(fadeCanvas.transform);
            fadeImageGo.transform.localScale = Vector3.one;
            fadeImageGo.GetComponent<RectTransform>().anchorMin = Vector2.zero;
            fadeImageGo.GetComponent<RectTransform>().anchorMax = Vector2.one;
            fadeImageGo.GetComponent<RectTransform>().offsetMin = Vector2.zero;
            fadeImageGo.GetComponent<RectTransform>().offsetMax = Vector2.zero;
        }


        /// <summary>
        /// TODO Take into account multiple canvasObjects
        /// Tries to find an image to use for fading if it was created with another FadeUI. If not creates a new one
        /// </summary>
        /// <exception cref="Exception">No canvas in the scene to fade</exception>
        private static void FindFadeImage()
        {
            GetCanvas();

            if (fadeCanvas == null) throw new Exception("No Canvas found. No Fade UI will be created.");
            fadeCanvas.sortingOrder = 10; // ensuring its on top of every other canvas

            GameObject fadeImageGo = GameObject.Find("FadeImage");
            // No Fade was found so a new one will be created
            if (fadeImageGo == null)
            {
                CreateFadeImage();
                return;
            }

            fadeTween = fadeImageGo.GetComponent<Image>();
            fadeTween.transform.SetAsLastSibling();
        }
        /// <summary>
        /// Finds the canvas in the scene to create the fade image. Tries to find a canvas with the FadeUI component
        /// </summary>
        /// <exception cref="Exception"></exception>
        private static void GetCanvas()
        {
            if (fadeCanvas != null) return;

            UnityEngine.Object[] canvasObjects = FindObjectsOfType(typeof(Canvas));

            if (canvasObjects == null || canvasObjects.Length <= 0) throw new Exception("No Canvas found. No Fade UI will be created.");

            Canvas[] canvasArray = Array.ConvertAll(canvasObjects, c => (Canvas)c);
            fadeCanvas = canvasArray[0];  //  Set the canvas to the first one for fallback reasons
            for (int i = 0; i < canvasArray.Length; i++)
            {
                if (canvasArray[i].GetComponent<FadeUI>() != null)
                {
                    fadeCanvas = canvasArray[i];
                    return;
                }
            }
        }
        void Awake()
        {
            StaticFadeDuration = fadeDuration;
            StaticFadeImageColor = fadeImageColor;
            FindFadeImage();
            SetAlpha(1);
            CoroutineHandler.ExecuteActionAfter(this, FadeIn, waitTimeToInitFade);
        }
        /// <summary>
        /// Static method to call the Fade Out from anywhere in the game. NOT triggering the events. Purely cosmetic.
        /// </summary>
        /// <param name="duration"> -1 if using the default duration of the instantiated object. If a different duration is desired </param>
        /// <param name="color"> Color for the image to fade </param>
        public static void FadeIn(float duration = -1, Color? color = null)
        {
            FindFadeImage();
            duration = duration == -1 ? StaticFadeDuration : duration;
            color = color == null ? Color.black : color;
            fadeTween.color = color.Value;
            Fade(1, 0, false, duration);
        }
        /// <summary>
        /// Static method to call the Fade Out from anywhere in the game. NOT triggering the events. Purely cosmetic.
        /// </summary>
        /// <param name="duration"> -1 if using the default duration of the instantiated object. If a different duration is desired </param>
        /// <param name="color"> Color for the image to fade </param>
        public static void FadeOut(float duration = -1, Color? color = null)
        {
            FindFadeImage();
            duration = duration == -1 ? StaticFadeDuration : duration;
            color = color == null ? Color.black : color;
            fadeTween.color = color.Value;
            Fade(0, 1, true, duration);
        }

        [Button("Fade In",EButtonEnableMode.Playmode)]
        public void FadeIn()
        {
            OnFadeInStarted?.Invoke();
            Fade(1, 0, false, fadeDuration);
            CoroutineHandler.ExecuteActionAfter(this,() => { OnFadeInFinished?.Invoke(); }, fadeDuration);
        }
        [Button("Fade Out", EButtonEnableMode.Playmode)]
        public void FadeOut()
        {
            OnFadeOutStarted?.Invoke();
            Fade(0, 1, true, fadeDuration);
            CoroutineHandler.ExecuteActionAfter(this, () => { OnFadeOutFinished?.Invoke(); }, fadeDuration);
        }

        private static void Fade(float from, float to, bool overrideAlphaSet = false, float duration = 1)
        {
            SetAlpha(from);
            if (overrideAlphaSet) SetAlpha(to);
            fadeTween.CrossFadeAlpha(from, 0, true);
            fadeTween.CrossFadeAlpha(to, duration, false);
        }

        private static void SetAlpha(float newAlpha)
        {
            Color newColor = fadeTween.color;
            newColor.a = newAlpha;
            fadeTween.color = newColor;
        }

        public static float GetFadeDuration() => StaticFadeDuration;
    }
}
