using System;
using UnityEngine;

namespace TamarilloTools
{
    /// <summary>
    /// Class resembling the widget switcher from Unreal Engine.
    /// This class will activate and deactivate its children in the hierarchy given an index
    /// Use cases are: tab system in a menu or tab system in an inventory screen.
    /// </summary>
    public class UIObjectSwitcher : MonoBehaviour
    {
        public enum NavigationType
        {
            Wrap,
            Clamp
        }
        protected GameObject currentActiveObject;
        [field: SerializeField] public NavigationType SwitcherNavigationType = NavigationType.Wrap;
        [field: SerializeField] public int CurrentIndex { get; private set; }

        /// <summary>
        /// Returns the total number of items inside the object switcher
        /// </summary>
        public int GetNumberOfItems
        {
            get => transform.childCount;
        }

        // Start is called before the first frame update
        protected virtual void Start()
        {
            SetInitialActiveObject();
        }

        protected virtual void OnValidate()
        {
            SetInitialActiveObject();
        }

        /// <summary>
        /// Deactivates all items and activates the item given the current index
        /// </summary>
        /// <exception cref="Exception"> Throws Exception if the index is invalid or if there are no child object in the switcher</exception>
        private void SetInitialActiveObject()
        {
            if (transform.childCount <= 0) return;
            if (CurrentIndex < 0 || CurrentIndex >= GetNumberOfItems) throw new Exception("Invalid Index for switcher");

            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }

            currentActiveObject = transform.GetChild(CurrentIndex).gameObject;
            currentActiveObject.SetActive(true);
        }

        /// <summary>
        /// Deactivates the current active object and activates a new one given a new index.
        /// Also sets the new index as current index
        /// </summary>
        /// <param name="newIndex"> The index of the item to activate</param>
        public virtual void SetActiveObjectIndex(int newIndex)
        {
            currentActiveObject?.SetActive(false);
            SetIndexByNavigationReq(newIndex);
            currentActiveObject = transform.GetChild(CurrentIndex).gameObject;
            currentActiveObject.SetActive(true);
        }

        /// <summary>
        /// Deactivates de current active object and activates a new one passing the object directly.
        /// It also sets the current index to a new one given the new object position in hierarchy
        /// </summary>
        /// <param name="newObject"> The object to activate inside the switcher</param>
        public virtual void SetActiveObject(GameObject newObject)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).gameObject == newObject)
                {
                    SetIndexByNavigationReq(i);
                    SetActiveObjectIndex(CurrentIndex);
                }
            }
        }

        /// <summary>
        /// Gets the current active gameobject inside the switcher
        /// </summary>
        /// <returns> Retusn the currentActiveGameobject</returns>
        public GameObject GetActiveObject()
        {
            return currentActiveObject;
        }

        /// <summary>
        /// Retusn the object at an index.
        /// The object doesn't need to be active
        /// </summary>
        /// <param name="index"> The index of the object to return</param>
        /// <returns> The object at the position "index" from the hierarchy</returns>
        public GameObject GetObjectAtIndex(int index)
        {
            index = index < 0 ? transform.childCount - 1 : index;
            index = SwitcherNavigationType == NavigationType.Wrap
                ? index % transform.childCount
                : Mathf.Clamp(index, 0, transform.childCount - 1);
            return transform.GetChild(index).gameObject;
        }

        private void SetIndexByNavigationReq(int newIndex)
        {
            int wrapIndex = newIndex < 0 ? transform.childCount - 1 : newIndex;
            CurrentIndex = SwitcherNavigationType == NavigationType.Wrap
                ? wrapIndex % transform.childCount
                : Mathf.Clamp(newIndex, 0, transform.childCount - 1);
        }
    }
}
