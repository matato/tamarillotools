using NaughtyAttributes;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TamarilloTools
{
    [RequireComponent(typeof(UIObjectSwitcher))]
    [RequireComponent(typeof(Mask))]
    [RequireComponent(typeof(Image))]
    public class UICarousel : Selectable
    {
        //[Header("Nav Bar")]
        //[SerializeField] private GameObject carouselNavBar; // #TODO Create and setup nav bar
        [Header("Animation")]
        [SerializeField] private bool useAnimation;
        [SerializeField] private float animationTime;
        //[Header("Auto Scroll")]
        //[SerializeField] private bool autoScroll; // #TODO Set up the auto scroll
        //[ShowIf("autoScroll")]
        //[SerializeField] private float autoScrollSpeed;

        public UnityEvent OnItemChanged;
        public UnityEvent OnCarouselSelected;
        public UnityEvent OnCarouselDeselected;

        private UIObjectSwitcher switcher;
        private RectTransform selectedObjectTransform;
        private RectTransform nextObjectTransform;
        private RectTransform prevObjectTransform;

        protected override void Awake()
        {
            base.Awake();
            switcher = GetComponent<TamarilloTools.UIObjectSwitcher>();
            SetChildrenInitialRectTransform();
        }
#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();
            SetChildrenInitialRectTransform();
        }
#endif
        private void UpdateRectReferences()
        {
            if(switcher == null) switcher = GetComponent<TamarilloTools.UIObjectSwitcher>();
            int index = switcher.CurrentIndex;
            selectedObjectTransform = switcher.GetActiveObject().transform as RectTransform;
            nextObjectTransform = switcher.GetObjectAtIndex(index + 1).transform as RectTransform;
            prevObjectTransform = switcher.GetObjectAtIndex(index - 1).transform as RectTransform;
        }

        private void SetChildrenInitialRectTransform()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                // Place all children pivot in the middle for movement calculations
                ((RectTransform)transform.GetChild(i)).pivot = new Vector2(0.5f, 0.5f);
            }
        }

        public virtual void NextPage()
        {
            UpdateRectReferences();

            if (LeanTween.isTweening(selectedObjectTransform) ||
                (switcher.CurrentIndex == switcher.GetNumberOfItems - 1 && switcher.SwitcherNavigationType == UIObjectSwitcher.NavigationType.Clamp)) return;

            if (!useAnimation)
            {
                nextObjectTransform.anchoredPosition = selectedObjectTransform.anchoredPosition;
                NextPageInternal();
            }
            else
            {
                nextObjectTransform.gameObject.SetActive(true);
                SlideAnimation(selectedObjectTransform, nextObjectTransform, ((RectTransform)transform).rect.width, NextPageInternal);
            }

        }

        public virtual void PreviousPage()
        {
            UpdateRectReferences();

            if (LeanTween.isTweening(selectedObjectTransform) ||
                (switcher.CurrentIndex == 0 && switcher.SwitcherNavigationType == UIObjectSwitcher.NavigationType.Clamp)) return;

            if (!useAnimation)
            {
                prevObjectTransform.anchoredPosition = selectedObjectTransform.anchoredPosition;
                PreviousPageInternal();
            }
            else
            {
                prevObjectTransform.gameObject.SetActive(true);
                SlideAnimation(selectedObjectTransform, prevObjectTransform, -((RectTransform)transform).rect.width, PreviousPageInternal);
            }

        }

        private void NextPageInternal()
        {
            int index = switcher.CurrentIndex;
            switcher.SetActiveObjectIndex(++index);
            OnItemChanged?.Invoke();
        }

        private void PreviousPageInternal()
        {
            int index = switcher.CurrentIndex;
            switcher.SetActiveObjectIndex(--index);
            OnItemChanged?.Invoke();
        }

        public override Selectable FindSelectableOnLeft()
        {
            if (navigation.mode == Navigation.Mode.Automatic)
                return null;
            return base.FindSelectableOnLeft();
        }

        public override Selectable FindSelectableOnRight()
        {
            if (navigation.mode == Navigation.Mode.Automatic)
                return null;
            return base.FindSelectableOnRight();
        }

        public override void Select()
        {
            base.Select();
            OnCarouselSelected?.Invoke();
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);
            OnCarouselSelected?.Invoke();
        }

        public override void OnDeselect(BaseEventData eventData)
        {
            base.OnDeselect(eventData);
            OnCarouselDeselected?.Invoke();
        }

        private void SlideAnimation(RectTransform transformToSlideOut, RectTransform transformToSlideIn, float distanceSigned, Action onComplete)
        {
            //Set the previous one to be behind of the selected one
            transformToSlideIn.anchoredPosition = new Vector2(transformToSlideOut.anchoredPosition.x + distanceSigned, transformToSlideIn.anchoredPosition.y);

            LeanTween.move(transformToSlideOut, new Vector3(transformToSlideOut.anchoredPosition.x - distanceSigned, transformToSlideOut.anchoredPosition.y, 0), animationTime)
                .setOnComplete(onComplete);
            LeanTween.move(transformToSlideIn, new Vector3(transformToSlideIn.anchoredPosition.x - distanceSigned, transformToSlideIn.anchoredPosition.y, 0), animationTime);
        }

        public int GetActiveIndex() => switcher.CurrentIndex;

        public GameObject GetActiveObject() => switcher.GetActiveObject();

        public void SetActiveIndex(int index) => switcher.SetActiveObjectIndex(index);

        public void SetActiveGameObject(GameObject newObject) => switcher.SetActiveObject(newObject);
    }
}
