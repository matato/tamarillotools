﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TamarilloTools
{
    /// <summary>
    /// This is a generic Serializable Dictionary that can be used to store whatever data you want to store.
    /// Both the key and the value can be anything. You just have to derive from the abstract class.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [System.Serializable]
    public class CustomDictionaryEntry<F, T>
    {
        public F Key;
        public T Value;
    }
    /// <summary>
    /// The database will be able to hold a generic dictionary.
    /// To use it just derive from the abstract class
    /// </summary>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="T"></typeparam>
    /// Commented the asset menu since it has to be overriden in the child class
    // [CreateAssetMenu(fileName = "New Database", menuName = "Tamarillo Tools/Create New Database", order = 2)]
    public abstract class DictionaryDatabase<F, T> : ScriptableObject
    {
        public List<CustomDictionaryEntry<F, T>> ObjectDatabase;

        public T GetByKey(F searchKey)
        {
            for (int i = 0; i < ObjectDatabase.Count; i++)
            {
                if (ObjectDatabase[i].Key.Equals(searchKey))
                {
                    return ObjectDatabase[i].Value;
                }
            }
            return default(T);
        }

        public T GetRandomObject(int startingIndex)
        {
            int randomEntry = UnityEngine.Random.Range(startingIndex, ObjectDatabase.Count);
            return ObjectDatabase[randomEntry].Value;
        }
    }

    public abstract class SimpleDatabase<T> : ScriptableObject
    {
        public List<T> ObjectDatabase;

        public T GetByKey(int searchIndex)
        {
            if (ObjectDatabase.Count <= 0) throw new Exception("Database is empty");
            if (ObjectDatabase == null) throw new Exception("List in database was not initialized");
            if (searchIndex < 0 || searchIndex >= ObjectDatabase.Count) throw new Exception("Invalid Index");
            
            return ObjectDatabase[searchIndex];
        }

        public T GetRandomObject(int startingIndex)
        {
            int randomEntry = UnityEngine.Random.Range(startingIndex, ObjectDatabase.Count);
            return ObjectDatabase[randomEntry];
        }
    }
}
