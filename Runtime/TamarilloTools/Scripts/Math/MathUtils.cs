using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TamarilloTools
{
    public class MathUtils
    {
        public static float EaseInQuart(float t)
        {
            return Mathf.Pow(Mathf.Clamp01(t), 4f);
        }

        public static float InverseEaseInQuart(float x)
        {
            return Mathf.Pow(Mathf.Clamp01(x), 0.25f);
        }
    }

}
